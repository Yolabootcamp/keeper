import "./styles/style.css";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Container from "./components/Container/Container";
import { ThemeProvider } from "./Theme/Theme";

function App() {
  return (
    <div className="App">
      <ThemeProvider>
        <Header />
        <Container />
        <Footer />
      </ThemeProvider>
    </div>
  );
}

export default App;

