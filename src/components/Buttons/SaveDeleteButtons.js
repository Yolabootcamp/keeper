import React from "react";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";

export default function SaveDeleteButtons({ handleEditNote, handleDelete }) {
  return (
    <div>
      <EditIcon
        fontSize="small"
        onClick={handleEditNote}
        className="btn-color"
      />

      <DeleteIcon
        fontSize="small"
        onClick={handleDelete}
        className="btn-color"
      />
    </div>
  );
}
