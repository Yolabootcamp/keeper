import React from "react";
import SaveIcon from "@mui/icons-material/Save";
import CancelIcon from "@mui/icons-material/Cancel";
import PaletteIcon from "@mui/icons-material/Palette";

export default function EditButtons({
  handleSave,
  handleCancel,
  handleColorON,
}) {
  return (
    <div>
      <PaletteIcon className="icon-color" onClick={handleColorON} />
      <SaveIcon fontSize="small" onClick={handleSave} className="save-btn" />
      <CancelIcon
        fontSize="small"
        onClick={handleCancel}
        className="cancel-btn"
      />
    </div>
  );
}
