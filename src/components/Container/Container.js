import React, { useState } from "react";
import Note from "../Note/Note";
import CreateArea from "../CreateArea/CreateArea";

export default function Container() {
  const [notes, setNotes] = useState([]);

  const addNote = (newNote) => {
    setNotes((prevNotes) => {
      return [...prevNotes, newNote];
    });
  };

  const deleteNote = (id) => {
    setNotes((prevNotes) => {
      return prevNotes.filter((note, index) => {
        return index !== id;
      });
    });
  };

  //const sorteNortes = notes.sort((a, b) => b.lastModified - a.lastModified);

  return (
    <div>
      <CreateArea addNote={addNote} />
      {notes.map((note, index) => {
        return (
          <Note
            key={index}
            id={index}
            title={note.title}
            content={note.content}
            date={note.date}
            deleteNote={deleteNote}
          />
        );
      })}
    </div>
  );
}
