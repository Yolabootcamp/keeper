import React from "react";
import { useTheme } from "../../Theme/Theme";
import { styled } from "@mui/system";

export default function Footer() {
  const year = new Date().getFullYear();

  const darkTheme = useTheme();

  const themeStyles = {
    backgroundColor: darkTheme ? "#0d0635" : "#f5ba13",
    border: darkTheme && "solid 1px #ccc",
    color: darkTheme ? "#fff" : "black",
  };

  return (
    <footer style={themeStyles}>
      <p> Copyright © {year} </p>
    </footer>
  );
}
