import React, { useState } from "react";
import SaveDeleteButtons from "../Buttons/SaveDeleteButtons";
import EditButtons from "../Buttons/EditButtons";
import { TwitterPicker } from "react-color";
import { useTheme } from "../../Theme/Theme";

export default function Note(props) {
  /**Hooks */
  const [editNote, setEditNote] = useState(false);

  const [editNewNote, setEditNewNote] = useState({
    editedTitle: props.title,
    editedContent: props.content,
    editedDate: props.date,
  });

  const [toSaveNote, setToSaveNote] = useState({
    newTitle: props.title,
    newContent: props.content,
    newDate: props.date,
  });

  const [cancelButton, setCancelButton] = useState(false);

  const [color, setColor] = useState("#fff");

  const [colorON, setColorON] = useState(false);
  /**Functions */

  const handleDelete = () => {
    props.deleteNote(props.id);
  };

  const handleEditNote = () => {
    setEditNote(true);
  };

  const handleNewTitle = (event) => {
    setEditNewNote((prevValue) => ({
      ...prevValue,
      editedTitle: event.target.value,
    }));
  };

  const handleNewContent = (event) => {
    setEditNewNote((prevValue) => ({
      ...prevValue,
      editedContent: event.target.value,
    }));
  };

  const handleSave = () => {
    setToSaveNote({
      newTitle: editNewNote.editedTitle,
      newContent: editNewNote.editedContent,
      newDate: editNewNote.editedDate,
    });
    setEditNote(false);
  };

  const handleCancel = () => {
    setCancelButton(true);
    setColor("#fff");
    setEditNote(false);
    setColorON(false);
  };

  const handleSaveColor = (color) => {
    setColor(color.hex);
    setColorON(false);
  };

  const handleColorON = () => {
    setColorON(true);
    //setCancelButton(false);
  };

  const darkTheme = useTheme();

  const themeStyles = {
    backgroundColor: "#eee",
    border: darkTheme && "solid 1px #fff",
    color: "black",
  };

  const inputStyles = {
    backgroundColor: "#fff",
    color: "black",
  };

  return (
    <div>
      {editNote ? (
        <div className="edition" style={{ backgroundColor: color }}>
          <div style={themeStyles}>
            <input
              type="text"
              name="editedTitle"
              value={editNewNote.editedTitle}
              placeholder="Title"
              onChange={handleNewTitle}
              className="title-input"
              style={inputStyles}
            />
            <input
              type="text"
              name="editedContent"
              value={editNewNote.editedContent}
              placeholder="Take a note..."
              className="content-input"
              onChange={handleNewContent}
              style={inputStyles}
            />
          </div>

          <div className="buttons">
            <div>
              {colorON ? (
                <TwitterPicker
                  className="colorPicker"
                  onChange={handleSaveColor}
                />
              ) : (
                false
              )}
            </div>
            <EditButtons
              handleSave={handleSave}
              handleCancel={handleCancel}
              handleColorON={handleColorON}
            />
          </div>
        </div>
      ) : (
        <div className="note" style={{ backgroundColor: color }}>
          <h1>
            {editNewNote.editedTitle === props.title
              ? props.title
              : toSaveNote.newTitle}
          </h1>
          <p>
            {editNewNote.editedContent === props.content
              ? props.content
              : toSaveNote.newContent}
          </p>
          <p className="data">{props.date}</p>
          <div className="buttons">
            <SaveDeleteButtons
              handleEditNote={handleEditNote}
              handleDelete={handleDelete}
            />
          </div>
        </div>
      )}
    </div>
  );
}
