import React from "react";
import { UilBrightness } from "@iconscout/react-unicons";
import { UilMoon } from "@iconscout/react-unicons";
import { useTheme } from "../../Theme/Theme";
import { useThemeUpdate } from "../../Theme/Theme";
import { yellow } from "@mui/material/colors";

export default function Header() {
  const darkTheme = useTheme();
  const toggleTheme = useThemeUpdate();

  const themeStyles = {
    backgroundColor: darkTheme ? '#0d0635' : "#f5ba13",
    border: darkTheme && "solid 1px #ccc",
    color: "#fff",
  };

  return (
    <header style={themeStyles}>
      <div>
        <h1>Keeper</h1>
        <div onClick={toggleTheme} className="dark-mode">
          {darkTheme ? (
            <UilMoon
              fontSize="large"
              sx={{ color: yellow[50] }}
              
            />
          ) : (
            <UilBrightness
              fontSize="large"
              sx={{ color: yellow[50] }}
            />
          )}
        </div>
      </div>
    </header>
  );
}
