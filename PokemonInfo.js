import React, { useState } from "react";
import axios from "axios";

export default function PokemonInfo() {
  const [pokemonName, setPokemonName] = useState("");

  const [searchPokemon, setSearchPokemon] = useState("");

  const [pokemonChosen, setPokemonChosen] = useState(false);

  const [pokemon, setPokemon] = useState({
    name: "",
    number: "",
    species: "",
    image: "",
    hp: "",
    attack: "",
    defense: "",
    speed: "",
    type: "",
    weight: "",
    height: "",
    gameIndice: "",
  });

  const message = "There is no pokemon for today! Sorry :( ";

  const handlePokemonName = (event) => {
    setPokemonName(event.target.value);
  };

  const handleSearchPokemon = () => {
    setSearchPokemon(() => {
      axios
        .get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`)
        .then((response) => {
          console.log(response.data);
          setPokemon({
            name: pokemonName,
            number: response.data.id,
            species: response.data.name,
            image: response.data.sprites.front_default,
            hp: response.data.stats[0].base_stat,
            attack: response.data.stats[1].base_stat,
            defense: response.data.stats[2].base_stat,
            speed: response.data.stats[5].base_stat,
            type: response.data.types[0].type.name,
            weight: response.data.weight,
            height: response.data.height,
            gameIndice: response.data.game_indices[0].game_index,
          });

          setPokemonChosen(true);
        })
        .catch((err) => {
          // setSearchPokemon(message);
          console.log(err.response.data);
        });
    });
  };

  return (
    <div className="pokemon-info">
      <div className="section-title">
        <h1>Pokédex</h1>

        <input
          type="text"
          onChange={handlePokemonName}
          value={pokemonName.toLowerCase()}
          placeholder="Pokémon name..."
        />
        <div>
          {pokemonName && (
            <button onClick={handleSearchPokemon}>Search Pokémon</button>
          )}
        </div>
      </div>
      <div className="display-section">
        {!pokemonChosen ? (
          <div className="display-section-h1">
            <h1>Choose a Pokémon</h1>
          </div>
        ) : (
          <>
            <div className="pokemon-details">
              <ul>
                <img src={pokemon.image} alt={pokemon.name} />
                <h1>{pokemon.name.toLocaleUpperCase()}</h1>
                <li>
                  {" "}
                  <h3> Number #{pokemon.number}</h3>
                </li>

                <li>
                  <h3> Species: {pokemon.species}</h3>
                </li>

                <li>
                  <h3> Type: {pokemon.type}</h3>
                </li>
                <li>
                  <h3> Hp: {pokemon.hp}</h3>
                </li>
                <li>
                  <h4> Attack: {pokemon.attack}</h4>
                </li>
                <li>
                  <h4> Defenses: {pokemon.defense}</h4>
                </li>
                <li>
                  <h4> Speed: {pokemon.speed}</h4>
                </li>
                <li>
                  <h4> Weight: {pokemon.weight}</h4>
                </li>
                <li>
                  <h4> Height: {pokemon.height}</h4>
                </li>
                <li>
                  <h4> Game Indices: {pokemon.gameIndice}</h4>
                </li>
              </ul>
            </div>
          </>
        )}
      </div>
    </div>
  );
}
